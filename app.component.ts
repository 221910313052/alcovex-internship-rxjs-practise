import { asNativeElements, Component } from '@angular/core';
import {Observable,from,of, fromEvent, interval} from 'rxjs'
import {map} from 'rxjs/operators'
import { ViewChild,ElementRef } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-app';
  //agentstring:string;
  students:Observable<string[]>=of(['mahesh','suresh','ramesh'])
  orders:Observable<string>=from('bread')
  newob=new Observable(function(newob){
    newob.next('rama')
    newob.next('sita')
    })
  
  @ViewChild('validate')
  validate:ElementRef

  btnclick()
  { /*using from operator */
    this.orders.subscribe(function(e){      
      
      console.log(e+'using from operator')
    })
    /* using normal observable*/
    this.newob.subscribe(function(m){console.log(m+'normal observable')})
    /*  using of operator*/
    this.students.subscribe(function(e){
      /*using interval operator */
      const intervalob=interval(500)
      intervalob.subscribe(function(y){console.log(e,y +'using interval and of operator')})
    })
  }
  validateclick()
  { /* using fromevent operator */ 
    const ob=fromEvent(this.validate?.nativeElement,'click')
    ob.subscribe(function(e){
      console.log(e+'fromevent operator')
    })
  }
    

  
}
